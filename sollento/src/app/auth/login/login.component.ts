import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(private router: Router, public http: HttpClient) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  goToRegister() {
    this.router.navigate(['/registration']);
  }

  logForm(){
    console.log(this.form.value);
    const form = this.form.value;
    const formData = new FormData();
    formData.append('email', form.email);
    formData.append('password', form.password);

    this.http.post('http://localhost:8000/login', formData)
      .subscribe(
        res => {
          console.log(res);
          localStorage.setItem('token', res['token']);
        },
        (error: HttpErrorResponse) => {
          console.log(error.status);
          console.log(error.error);
        }
      );
  }
}
