import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Router } from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  constructor(public http: HttpClient, public router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      fullName: new FormControl('', [Validators.required, Validators.minLength(5)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)])
    });
  }

  logForm() {
    console.log(this.form);

    const form = this.form.value;

    const formData = new FormData();
    formData.append('fullName', form.fullName);
    formData.append('email', form.email);
    formData.append('password', form.password);
    formData.append('isOwner', 'true');
    this.http.post('http://localhost:8000/register', formData)
      .subscribe((res: Response) => {
        console.log(res);
        this.router.navigate(['/login']);
      },
        (error: HttpErrorResponse) => {
          console.log(error.status);
          console.log(error.error);
        }
      );
  }

}
